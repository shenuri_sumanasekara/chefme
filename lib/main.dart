import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'dart:async';
import 'spalshGifScreen.dart';

void main() {
  runApp(ChefMe());
}

class ChefMe extends StatelessWidget {
  @override
  Widget build(BuildContext context1) {
    return MaterialApp(title: 'ChefMe', home: SplashScreen(), debugShowCheckedModeBanner: false);
  }
}




class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();

}


class _SplashScreenState extends State<SplashScreen> {
  void initState(){
    super.initState();
    Future.delayed(Duration(seconds: 3), (){
      Navigator.push(context, MaterialPageRoute(builder: (context)=> SplashGif()));
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black26,
      body: Container(
        child:Padding(
          padding: EdgeInsets.only(top:30,right: 4,left:4),
          child: Container(
              height: double.infinity,
              width: double.infinity,
              //padding: EdgeInsets.only(40.0),
              decoration: BoxDecoration(
                color: Colors.black26,backgroundBlendMode: BlendMode.colorBurn,
                  image: DecorationImage(
                      image:AssetImage('assets/images/cover2.jpg')
                  )

              ),
              child:Stack(
                alignment: Alignment.center,
                children: <Widget>[


                  Positioned(
                    top: 260,
                    child: Container(
                      child:Shimmer.fromColors(
                        period: Duration(milliseconds: 1500),
                        baseColor:Colors.white,
                        highlightColor: Colors.white30,
                        child: Text(" Chef Me " ,
                          style: TextStyle(
                              fontFamily: 'Pacifico',
                              fontSize: 50,
                              shadows: <Shadow>[
                                Shadow(
                                  blurRadius: 10,
                                  color: Colors.white,
                                )
                              ]
                          ),
                        ),
                      ),
                    ),
                  )
                ],

              )

          ),

        )

      ),

    );

  }

}

