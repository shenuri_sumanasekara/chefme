//import 'package:chefme/model/merchant.dart';
import 'dart:core';

import 'package:chefme/arpicoFood.dart';
import 'package:chefme/arpicoHome.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'home.dart';
import 'model/merchant.dart';
List<Merchant> merchantList = [];

class MerchantHome extends StatefulWidget {
  @override
  _MerchantHomeState createState() => _MerchantHomeState();
}

class _MerchantHomeState extends State<MerchantHome> {
 // get _merchantList => merchantList;


  @override

  void initState(){
    super.initState();
    getMerchants();
  }
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.blue[50],
       body: CustomScrollView(
         slivers: <Widget>[
           SliverGrid(
             gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
               crossAxisCount: 2,
//               crossAxisSpacing: 20,
//               mainAxisSpacing: 20,

             ),
             delegate:SliverChildBuilderDelegate(
                   (BuildContext context, index){
                 return Padding(
                   padding: EdgeInsets.only(top:20, left:20, right: 20),
                   child:MaterialButton(
                     height: 250,
                     shape: RoundedRectangleBorder(
                       borderRadius:  BorderRadius.circular(18),
                     ),

                     onPressed: () {
                       Navigator.of(context).push(MaterialPageRoute(builder: (context) =>
                           ArpicoFood()
                       ));
                     },
                     color: Colors.white,
                     child: Column(
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: <Widget>[

                           Container(
                             child: Image.network(merchantList[index].img,
                               height: 90,
                               width: 90,),
                           ),
                           Padding(
                               padding:EdgeInsets.only(top:20),
                               child:Text(merchantList[index].name,
                                 style: TextStyle(fontSize: 20,
                                     color: Colors.blueGrey),)
                           ),

                         ]
                     ),
                   ),
                );
               },
               childCount: merchantList.length,

             ) ,
           ),
         ],
       ),



//      body: GridView.builder(
//        padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
//         scrollDirection: Axis.vertical,
//          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, mainAxisSpacing: 20,
//            crossAxisSpacing: 20),
//          itemBuilder:(BuildContext context, int index) {
//          return MaterialButton(
//            height: 250,
//         shape: RoundedRectangleBorder(
//           borderRadius:  BorderRadius.circular(18),
//         ),
//                //padding: EdgeInsets.all(20),
//                onPressed: (){
//                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => MerchantHome()));
//                },
//                color: Colors.white,
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//                  children: <Widget>[
//
//                    Container(
//                      child: Image.network(merchantList[index].img,
//                      height: 90,
//                      width: 90,),
////                      height: 90,
////                      width: 90,
////                      decoration: BoxDecoration(
////                          image: DecorationImage(
////                            image: AssetImage(merchantList[index].img),
////                          ),
////                          borderRadius: BorderRadius.circular(100),
////                          border: Border.all(
////                              color: Color(0xFF21BFBD),
////                              width: 3,
////                              style: BorderStyle.solid
////                          )
////                      ),
//            ),
//              Padding(
//                padding:EdgeInsets.only(top:20),
//                child:Text(merchantList[index].name,
//                style: TextStyle(fontSize: 20,
//                    color: Colors.blueGrey),)
//              ),
//
//        ]
//            ),
//          );
//        },
//        itemCount:merchantList.length,
//        ),

    );
  }
}


Future<void> getMerchants() async {

  final fireStoreInstance = Firestore.instance;
  fireStoreInstance.collection("merchant").orderBy('name').getDocuments().then((querySnapshot) {
    merchantList.clear();
    querySnapshot.documents.forEach((result) {
      print(result.data);
      Merchant merchant = Merchant.fromMap(result.data);
      merchantList.add(merchant);

    });

  });

}
