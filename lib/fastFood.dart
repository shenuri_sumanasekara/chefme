import 'package:chefme/model/merchant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'merchantHome.dart';

List<Merchant> fastFoodList =[];
class FastFood extends StatefulWidget {
  @override
  _FastFoodState createState() => _FastFoodState();
}

class _FastFoodState extends State<FastFood> {
  @override

  // ignore: must_call_super
  void initState() {
    // TODO: implement initState
   getFastFood();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[50],
      body: GridView.builder(
        padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
        scrollDirection: Axis.vertical,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, mainAxisSpacing: 20,
            crossAxisSpacing: 20),
        itemBuilder:(BuildContext context, int index) {
          return MaterialButton(
            height: 250,
            shape: RoundedRectangleBorder(
              borderRadius:  BorderRadius.circular(18),
            ),
            //padding: EdgeInsets.all(20),
            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => MerchantHome()));
            },
            color: Colors.white,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Container(
                    child: Image.network(fastFoodList[index].img,
                      height: 90,
                      width: 90,),
//                      height: 90,
//                      width: 90,
//                      decoration: BoxDecoration(
//                          image: DecorationImage(
//                            image: AssetImage(merchantList[index].img),
//                          ),
//                          borderRadius: BorderRadius.circular(100),
//                          border: Border.all(
//                              color: Color(0xFF21BFBD),
//                              width: 3,
//                              style: BorderStyle.solid
//                          )
//                      ),
                  ),
                  Padding(
                      padding:EdgeInsets.only(top:20),
                      child:Text(fastFoodList[index].name,
                        style: TextStyle(fontSize: 20,
                            color: Colors.blueGrey),)
                  ),

                ]
            ),
          );
        },
        itemCount:fastFoodList.length,
      ),

    );
  }
}



Future<void> getFastFood(){
   final firestoreColletion = Firestore.instance;
   return firestoreColletion.collection('merchant').where('category',isEqualTo:'fastfood').getDocuments().then((querySnapShot) {
     fastFoodList.clear();
     querySnapShot.documents.forEach((element) {
       Merchant merchant = Merchant.fromMap(element.data);
       print(element.data);
       fastFoodList.add(merchant);
     }) ;
   });
}