import 'package:chefme/addRecipe.dart';
import 'package:chefme/login.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'home.dart';


class SplashGif extends StatefulWidget {
  @override
  _SplashGifState createState() => _SplashGifState();
}

class _SplashGifState extends State<SplashGif> {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.blueGrey,
      body: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top:20),
            child: SizedBox(
                child:Image.asset('assets/images/cove3.jpg')
            ),
          ),
          Padding(
            padding:EdgeInsets.only(top:97),
            child: SizedBox(
              height: 400,
              child: Image.asset('assets/images/girlGif.webp'),
           ),
          ),

          Padding(
            padding: EdgeInsets.only(top:392),
            child: SizedBox(
                child:Image.asset('assets/images/gif.jpg')
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 410,left:90),
            child:SizedBox(
                height: 70,
                width: 200,

                child:RaisedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) =>Login()));
                  },
                  color: Colors.white,
                  child: Text('Let''s Get Started' ,
                    style: TextStyle(fontFamily: 'Pacifico',
                        fontSize: 20,
                        color: Colors.blue),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18),
                      side: BorderSide(color: Colors.white)
                  ),
                )
            ),
          ),
        ],
      ),



    );



  }
}
