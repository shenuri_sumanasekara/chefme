import 'package:flutter/material.dart';

class Product{
   String docId;
   String name;
   int price;
   String category;
   String img;

   Product.fromMap(Map<String, dynamic>data){
     docId= data['docId'];
     name= data['name'];
     price= data['price'];
     category = data['category'];
     img = data['img'];
   }
}

