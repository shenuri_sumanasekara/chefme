import 'package:cloud_firestore/cloud_firestore.dart';


class Merchant {
  String docId;
  String name;
  String location;
  String img;
  String category;

  Merchant.fromMap(Map<String,dynamic> data){
    docId = data['docId'];
    name= data['name'];
    location= data['location'];
    img = data['img'];
    category = data['category'];

  }
}