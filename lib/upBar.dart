

import 'package:chefme/home.dart';
import 'package:chefme/model/merchant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'merchantHome.dart' as merchantHome;
import 'groceries.dart' as groceries;
import 'fastFood.dart' as fastFood;

//List <Merchant>  merchantList = [];

class UpBars extends StatefulWidget {
  @override
  _UpBarsState createState() => _UpBarsState();
}

class _UpBarsState extends State<UpBars>  with SingleTickerProviderStateMixin {
  TabController controller;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    controller= TabController(
      length: 3,
      vsync: this,
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            title: _titleRow(),
            pinned: true,
            floating: false,
            snap: false,
            expandedHeight: 140,
            flexibleSpace: FlexibleSpaceBar(
            background: Padding(
              padding: EdgeInsets.only(top: 90, left: 20),
              child:TabBar(
              isScrollable: true,
              unselectedLabelColor: Colors.white,
              labelColor: Colors.black26,
              tabs: <Tab>[
                Tab(
                  text: "All Categories",
                  icon: Icon(IconData(58819, fontFamily: 'MaterialIcons',)),
                ),
                Tab(
                  text: "Groceries",
                  icon: Icon(IconData( 59596, fontFamily: 'MaterialIcons')),
                ),
                Tab(
                  text: "Fast Food",
                  icon: Icon(IconData(58746, fontFamily: 'MaterialIcons')),
                ),

              ],
              controller: controller,
            ),
            ),
            ),

             ),


          SliverFillRemaining(
            child:TabBarView(
              controller: controller,
              children: <Widget>[
                merchantHome.MerchantHome(),
                groceries.Groceries(),
                fastFood.FastFood(),
              ],

            ),
          ),
//          SliverGrid(
//            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//              crossAxisCount: 2,
//              crossAxisSpacing: 20,
//              mainAxisSpacing: 20,
//
//            ),
//            delegate:SliverChildBuilderDelegate(
//                  (BuildContext context, index){
//                return MaterialButton(
//                  height: 250,
//                  shape: RoundedRectangleBorder(
//                    borderRadius:  BorderRadius.circular(18),
//                  ),
//
//                  onPressed: (){
//                    Navigator.of(context).push(MaterialPageRoute(builder: (context) =>Home()));
//                  },
//                  color: Colors.white,
//                  child: Column(
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      children: <Widget>[
//
//                        Container(
//                          child: Image.network(merchantList[index].img,
//                            height: 90,
//                            width: 90,),
//                        ),
//                        Padding(
//                            padding:EdgeInsets.only(top:20),
//                            child:Text(merchantList[index].name,
//                              style: TextStyle(fontSize: 20,
//                                  color: Colors.blueGrey),)
//                        ),
//
//                      ]
//                  ),
//                );
//
//              },
//              childCount: merchantList.length,
//
//            ) ,
//          ),
        ],
      ),
    );
  }
}

Widget _titleRow(){
  return Row(
    children: <Widget>[
      Text('Chef Me',
        style: TextStyle(fontFamily: 'Pacifico',
            fontSize: 24,
            color: Colors.white),
      ),
      Padding(
        padding: EdgeInsets.only(left: 83),
        child: Container(
          alignment: Alignment.topRight,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.search,color: Colors.white),
                iconSize: 26,
              ),
              IconButton(
                icon: Icon(Icons.shopping_cart,color: Colors.white),
                iconSize: 24,
              ),
            ],
          ),
        ),
      ),
    ],
  );
}
//Future<void> getMerchants() async {
//
//  final fireStoreInstance = Firestore.instance;
//  fireStoreInstance.collection("merchant").orderBy('name').getDocuments().then((querySnapshot) {
//    merchantList.clear();
//    querySnapshot.documents.forEach((result) {
//      print(result.data);
//      Merchant merchant = Merchant.fromMap(result.data);
//      merchantList.add(merchant);
//
//    });
//
//  });
//
//}
