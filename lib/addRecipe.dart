import 'package:chefme/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class AddRecipe extends StatefulWidget {
  @override
  _AddRecipeState createState() => _AddRecipeState();
}

class _AddRecipeState extends State<AddRecipe> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       backgroundColor: Color.fromRGBO(255, 119, 142,30),
       body: ListView(
         children: <Widget>[
           Container(
             child:Padding(
               padding: EdgeInsets.only(top:7),
               child:  Row(
                 children: <Widget>[
                   IconButton(
                     icon: Icon(Icons.arrow_back_ios),
                     color: Colors.white,
                     iconSize: 20,
                     onPressed: (){
                       Navigator.of(context).push(MaterialPageRoute(builder :(context) =>Home()));
                     },
                   ),
                   Text(
                     '    Add Your Own Recipe',
                     style: TextStyle(
                         color: Colors.white,
                         fontFamily: 'Pacifico',
                         fontSize: 25
                     ),
                   )
                 ],
               ),
             ),
             ),

           SingleChildScrollView(

             child:Padding(
               padding: EdgeInsets.only(top:10),
               child: Container(
                  height: 555,
                  decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(80),topRight: Radius.elliptical(250,250)
                  ),

                  ),


                child: Padding(
                  padding: EdgeInsets.only(top:40),
                 child:ListView(
                        children: <Widget>[
                        Container(
                        padding: EdgeInsets.only(top:30, left:20,right:80),
                        child: TextField(
                        decoration: InputDecoration(
                          labelText: 'Name',
                          hintText: 'ex: Maria Fernando',
                          enabledBorder: OutlineInputBorder(
                           // borderRadius: BorderRadius.circular(30),
                            borderSide: BorderSide(
                                color: Colors.pinkAccent ,width: 2),
                          ),

                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(70),
                            borderSide: BorderSide(
                                color: Colors.pink, width: 2.0),
                          ),

                        ),
                        ),

                        ) ,


                          Container(
                            padding: EdgeInsets.only(top:30, left:20,right:80),
                            child: TextField(
                              decoration: InputDecoration(
                                labelText: 'Recipe Name',
                                hintText: 'ex: Fried Rice',
                                enabledBorder: OutlineInputBorder(
                                  // borderRadius: BorderRadius.circular(30),
                                  borderSide: BorderSide(
                                      color: Colors.pinkAccent ,width: 2),
                                ),

                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(70),
                                  borderSide: BorderSide(
                                      color: Colors.pink, width: 2.0),
                                ),

                              ),
                            ),

                          ) ,

                    Container(
                      padding: EdgeInsets.only(top:30, left:20,right:20),
                      child: _buildTextField(),
                    ),
                        ],
                        ) ),
                )
                    )
                ),

         ],
       ),
    );
  }
}

Widget _buildTextField() {
  final maxLines =10;

  return Container(
    margin: EdgeInsets.all(12),
    height: maxLines * 20.0,
    child: TextField(
      keyboardType: TextInputType.multiline,
      maxLines: maxLines,
      decoration: InputDecoration(
        labelText: 'Ingreients',
        alignLabelWithHint: true,
        hintText: 'ex: Rice  2 bowls,                            '
            ' Carrot 100g,                                       '
            ' Leeks 100g                                          '
            ' Choped Big Onions 100g,                            '
            ' Oil 2 teaspoons',
        enabledBorder: OutlineInputBorder(
          // borderRadius: BorderRadius.circular(30),
          borderSide: BorderSide(
              color: Colors.pinkAccent ,width: 2),
        ),

        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(70),
          borderSide: BorderSide(
              color: Colors.pink, width: 2.0),
        ),

      ),
    ),
  );
}