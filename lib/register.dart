import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'home.dart';

String  firstName, lastName, email, phone,password,pass;
class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();

}

class _RegisterState extends State<Register> {
  TextEditingController emailController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passController = TextEditingController();

  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;

    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: true,
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 290, 0),
            child: Container(
              height: 120,
              decoration: BoxDecoration(
                color: Colors.green[100],
                borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(95)),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Container(
              alignment: Alignment(0.5, 0.5),
              height: 50,
              width: 120,
              decoration: BoxDecoration(
                color: Colors.green,
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(95),
                    bottomRight: Radius.circular(95)),
              ),
            ),
          ),
          Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(top: 50, left: 70),
              height: 200,
              width: 280,
              child: ClipOval(
                child: Image.asset(
                    'assets/images/logo2.png',
                    colorBlendMode: BlendMode.color),
              )

          ),


          Padding(
            padding: EdgeInsets.only(top: 200),
            child: Form(
              key: _formKey,
              child: ListView(
                children: [
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 5, 10),
                        width: 180,
                        height: 60,
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          controller: _firstNameController,

                        decoration: InputDecoration(
                            fillColor: Colors.lightGreen[200].withOpacity(0.2),
                            filled: true,
                            prefixIcon: Icon(
                              Icons.person, color: Colors.green,),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.deepPurple,
                                    style: BorderStyle.solid,
                                    width: 2),
                                borderRadius: BorderRadius.circular(30.0)

                            ),
                            labelText: 'First Name',
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(5, 0, 10, 10),
                        width: 180,
                        height: 60,
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter some text';
                            }
                            return null;
                          },
                          controller: lastNameController,
                          decoration: InputDecoration(
                            fillColor: Colors.lightGreen[200].withOpacity(0.2),
                            filled: true,
                            prefixIcon: Icon(
                              Icons.person, color: Colors.green,),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.deepPurple,
                                    style: BorderStyle.solid,
                                    width: 2),
                                borderRadius: BorderRadius.circular(30.0)

                            ),
                            labelText: 'Last Name',
                          ),
                        ),
                      ),
                    ],
                  ),

                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(10, 0, 5, 10),
                        width: 180,
                        height: 60,
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please enter the mobile number';
                            }
                            return null;
                          },
                          controller: phoneController,
                          decoration: InputDecoration(
                            fillColor: Colors.lightGreen[200].withOpacity(0.2),
                            filled: true,
                            prefixIcon: Icon(Icons.phone, color: Colors.green,),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.deepPurple,
                                    style: BorderStyle.solid,
                                    width: 2),
                                borderRadius: BorderRadius.circular(30.0)

                            ),
                            labelText: 'Mobile',
                          ),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(5, 0, 10, 10),
                        width: 180,
                        height: 60,
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'E-mail is required';
                            }
                            return null;
                          },
                          controller: emailController,
                          decoration: InputDecoration(
                            fillColor: Colors.lightGreen[200].withOpacity(0.2),
                            filled: true,
                            prefixIcon: Icon(Icons.mail, color: Colors.green,),
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.deepPurple,
                                    style: BorderStyle.solid,
                                    width: 2),
                                borderRadius: BorderRadius.circular(30.0)

                            ),
                            labelText: 'E-mail',
                          ),
                        ),
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(50, 0, 50, 10),
                    width: 180,
                    height: 60,
                    child: TextFormField(
                      validator: (String value) {
                        if (value.isEmpty) {
                          return 'Please enter some a  Password';
                        }
                        if (value.length < 5) {
                          return 'Enter a password with atleast 6 characters';
                        }
                        return null;
                      },
                      controller: passwordController,
                      obscureText: true,
                      decoration: InputDecoration(
                        fillColor: Colors.lightGreen[200].withOpacity(0.2),
                        filled: true,
                        prefixIcon: Icon(Icons.lock, color: Colors.green,),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.deepPurple,
                                style: BorderStyle.solid,
                                width: 2),
                            borderRadius: BorderRadius.circular(30.0)

                        ),
                        labelText: 'Password',
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(50, 0, 50, 10),
                    width: 180,
                    height: 60,
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter the password';
                          // ignore: unrelated_type_equality_checks
                        }
//                            }if(value != passwordController){
//                              return 'Passwords does not match';
//                            }
                        return null;
                      },
                      controller: passController,
                      obscureText: true,
                      decoration: InputDecoration(
                        fillColor: Colors.lightGreen[200].withOpacity(0.2),
                        filled: true,
                        prefixIcon: Icon(Icons.person, color: Colors.green,),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.deepPurple,
                                style: BorderStyle.solid,
                                width: 2),
                            borderRadius: BorderRadius.circular(30.0)

                        ),
                        labelText: 'Re-Enter Password',
                      ),
                    ),
                  ),

                ],
              ),


            ),


          ),
          Padding(
              padding: EdgeInsets.only(top: 480, left: 50, right: 50),
              child: ButtonTheme(
                height: 50,
                minWidth: 280,
                buttonColor: Colors.green,
                child: RaisedButton(
                  color: Colors.lightGreen[300],
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      signUP();
                    }
                  },
                  child: Text('Login', style: TextStyle(fontFamily: 'Pacifico',
                      fontSize: 20,
                      color: Colors.white),),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                ),
              )

          ),
        ],


      ),


    );
  }


  Future<void> signUP() async {
    firstName = _firstNameController.text;
    email = emailController.text;
    lastName = lastNameController.text;
    phone = phoneController.text;
    password = passwordController.text;
    pass = passController.text;

    final FirebaseUser firebaseUser = (await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password)).user;
    String uid = firebaseUser.uid;
    registerUser(uid, firstName, lastName, email, phone, password);
    Navigator.of(context).push(
        MaterialPageRoute(builder: (context) => Home()));

  }
}

   registerUser(String uid, String firstName, String lastName, String email, String phone, String password) {
   return Firestore.instance.collection('User').document(uid).setData({'firstName':firstName,'lastName':lastName,'email': email, 'phone':phone,'password': password});
   }
  
  



