import 'package:chefme/register.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'home.dart';
import 'home.dart';
import 'register.dart';
import 'register.dart';
import 'register.dart';


class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();

}

class _LoginState extends State<Login> {
  String email,password;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: loginForm(),
    );

}


Widget loginForm(){

  return  Stack(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.fromLTRB(0, 0, 290, 0),
        child: Container(
          height: 100,
          decoration: BoxDecoration(
            color: Colors.green[100],
            borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(95)),
          ),
        ),
      ),

      Container(
        alignment: Alignment.topRight,
        padding: EdgeInsets.only(top: 50, left: 27),
        height: 230,
        width: 330,
        child: ClipOval(
            child: ColorFiltered(
              colorFilter: ColorFilter.mode(Colors.greenAccent[200], BlendMode.color) ,
              child: Image.asset(
                  'assets/images/logingirl.jpg',
                  colorBlendMode: BlendMode.color),
            )
        ),
      ),
      Container(
        alignment: Alignment.topRight,
        padding: EdgeInsets.only(right: 159, top: 100),
        height: 400,
        width: 380,
        child: ClipOval(
          child: Image.asset('assets/images/mom.jpg'),
        ),
      ),

      Padding(
          padding: EdgeInsets.only(top: 280),
          child: Form(
            key: _formKey,
            child: ListView(
              children: [
                Container(
                  padding: EdgeInsets.fromLTRB(50, 0, 50, 0),
                  child: TextFormField(
                    controller: emailController,
                    decoration: InputDecoration(
                        fillColor: Colors.lightGreen[200].withOpacity(0.2),
                        filled: true,
                        prefixIcon: Icon(Icons.email, color: Colors.green,),
                        border: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white,
                                style: BorderStyle.solid,
                                width: 2),
                            borderRadius: BorderRadius.circular(30.0)
                        ),

                        labelText: 'Email'
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(50, 10, 50, 10),

                  child: TextFormField(
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Please enter some text';
                      }
                      return null;
                    },
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                      fillColor: Colors.lightGreen[200].withOpacity(0.2),
                      filled: true,
                      prefixIcon: Icon(Icons.lock, color: Colors.green,),
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.deepPurple,
                              style: BorderStyle.solid,
                              width: 2),
                          borderRadius: BorderRadius.circular(30.0)

                      ),
                      labelText: 'Password',
                    ),
                  ),
                ),
              ],
            ),

          )
      ),
      Padding(
          padding: EdgeInsets.only(top: 485, left: 50, right: 50),
          child: ButtonTheme(
            height: 50,
            minWidth: 280,
            buttonColor: Colors.green,
            child: RaisedButton(
              color: Colors.lightGreen[300],
              onPressed: (){
                if (_formKey.currentState.validate()) {
                  signIn(context);
                }

              },
              child: Text('Login', style: TextStyle(fontFamily: 'Pacifico',
                  fontSize: 20,
                  color: Colors.white),),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30),
              ),
            ),
          )

      ),
      Padding(
        padding: EdgeInsets.only(top: 535, left: 65, right: 40),
        child:  Row(
          children: [
            Text(
                "Don't have an account ?",
                style: TextStyle(fontFamily: 'Pacifico',
                    fontSize: 17,
                    color: Colors.green)
            ),
            GestureDetector(
              onTap:() {
                Navigator.push(context,MaterialPageRoute(builder: (context) => Register()));
              },
              child:Text(
                  " Sign-Up",
                  style: TextStyle(fontFamily: 'Pacifico',
                      fontSize: 20,
                      color: Colors.green)
              ),
            )
          ],
        ),
      )

    ],
  );

}
  Future< void> signIn(BuildContext context) async{
    email = emailController.text;
    password = passwordController.text;

    try {
      final FirebaseUser firebaseUser = (await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password:password)).user;
      Navigator.push(context,MaterialPageRoute (builder: (context)=> Home(uid : firebaseUser.uid)));

    }catch(e){
      final snackBar= SnackBar(
        backgroundColor: Colors.teal,
        content: Text('Error Login'),
        duration: new Duration(seconds: 3),
      );
   _scaffoldKey.currentState.showSnackBar(snackBar);



    }



  }
  }

