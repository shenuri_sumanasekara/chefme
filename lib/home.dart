import 'package:chefme/addRecipe.dart';
import 'package:chefme/riceHome.dart';
import 'package:chefme/upBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'fastFood.dart';
import 'tabBar.dart';
import 'drawerNavigator.dart';
import 'merchantHome.dart';


class Home extends StatefulWidget {
  String uid;
  Home({this.uid});
  @override
  _HomeState createState() => _HomeState(uid);
}
final GlobalKey<ScaffoldState> _scaffoldKeys = new GlobalKey<ScaffoldState>();


class _HomeState extends State<Home> {
   void initState(){
     getFastFood();
   }
  String uid;
  _HomeState(this.uid);


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKeys,
      backgroundColor: Color(0xFF21BFBD),
      drawer: NavDrawer(uid: uid),
      body: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 5,left:15),
              child:Row(

                children: <Widget>[
                  IconButton(
                      icon: Icon(Icons.menu),
                      color: Colors.white,
                      onPressed:()=> _scaffoldKeys.currentState.openDrawer()),

                  Padding(
                      padding: EdgeInsets.only(left: 200),
                      child: Container(
                        alignment: Alignment.topRight,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            IconButton(
                              icon: Icon(Icons.notifications),
                              color:Colors.white,
                              onPressed: (){},
                            ),
                            IconButton(
                              icon: Icon(Icons.settings),
                              color: Colors.white,
                              onPressed: (){},
                            )
                          ],
                        ),

                      )
                  ),

                ],
              ),
            ),

            Padding(
              padding: EdgeInsets.only(top: 0),
              child: Row(
                children: <Widget>[
                  Text('  Chef Me',
                    style: TextStyle(fontFamily: 'Pacifico',
                        fontSize: 40,
                        color: Colors.white),
                  ),
                  IconButton(
                    padding: EdgeInsets.only(left:20),
                    icon: Icon(Icons.fastfood),
                    color:Colors.white,
                    iconSize: 40,
                    onPressed: (){},

                  )
                ],
              ),
            ),


        SingleChildScrollView(

         child: Container(
            height: 455,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(85.0),topRight: Radius.circular(75)),
            ),
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top:20, left:50),
                  child:Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          MaterialButton(
                            onPressed: (){
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) =>TabBars()));
                            },
                            child: Container(
                              height: 85,
                              width: 85,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage('assets/images/rice.jpg'),
                                  ),
                                  borderRadius: BorderRadius.circular(100),
                                  border: Border.all(
                                      color: Color(0xFF21BFBD),
                                      width: 3,
                                      style: BorderStyle.solid
                                  )
                              ),
                            ),

                          ),
                          Text(
                            'Rice',
                            style: TextStyle(fontFamily: 'Pacifico',
                                fontSize: 20,
                                color: Colors.blueGrey),
                          ),
                        ],
                      ),

                      Padding(
                          padding: EdgeInsets.only(left:40),
                          child: Column(
                            children: <Widget>[
                              MaterialButton(
                                onPressed: (){
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => TabBars()));
                                },
                                child: Container(
                                  height:85,
                                  width: 85,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage('assets/images/noodles3.jpg'),
                                      ),
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                          color: Color(0xFF21BFBD),
                                          width: 3,
                                          style: BorderStyle.solid
                                      )
                                  ),
                                ),

                              ),

                              Text(
                                'Noodles',
                                style: TextStyle(fontFamily: 'Pacifico',
                                    fontSize: 20,
                                    color: Colors.blueGrey),
                              ),

                            ],
                          )
                      ),
                    ],

                  ),



                ),

                Padding(
                  padding: EdgeInsets.only(top:20, left:50),
                  child:Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          MaterialButton(
                            onPressed: (){
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => UpBars()));
                            },
                            child: Container(
                              height: 85,
                              width: 85,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage('assets/images/pasta.jpg'),
                                  ),
                                  borderRadius: BorderRadius.circular(100),
                                  border: Border.all(
                                      color: Color(0xFF21BFBD),
                                      width: 3,
                                      style: BorderStyle.solid
                                  )
                              ),
                            ),

                          ),
                          Text(
                            'Pasta',
                            style: TextStyle(fontFamily: 'Pacifico',
                                fontSize: 20,
                                color: Colors.blueGrey),
                          ),
                        ],
                      ),

                      Padding(
                          padding: EdgeInsets.only(left:40),
                          child: Column(
                            children: <Widget>[
                              MaterialButton(
                                onPressed: (){
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => RiceHome()));
                                },
                                child: Container(
                                  height:85,
                                  width: 85,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                        image: AssetImage('assets/images/soup.jpg'),
                                      ),
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                          color: Color(0xFF21BFBD),
                                          width: 3,
                                          style: BorderStyle.solid
                                      )
                                  ),
                                ),

                              ),

                              Text(
                                'Soup',
                                style: TextStyle(fontFamily: 'Pacifico',
                                    fontSize: 20,
                                    color: Colors.blueGrey),
                              ),

                            ],
                          )
                      ),
                    ],

                  ),



                ),


                Padding(
                  padding: EdgeInsets.only(top:20, left:50),
                  child:Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          MaterialButton(
                            onPressed: (){
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => RiceHome()));
                            },
                            child: Container(
                              height: 85,
                              width: 85,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage('assets/images/bun2.jpg'),
                                  ),
                                  borderRadius: BorderRadius.circular(100),
                                  border: Border.all(
                                      color: Color(0xFF21BFBD),
                                      width: 3,
                                      style: BorderStyle.solid
                                  )
                              ),
                            ),

                          ),
                          Text(
                            'Buns',
                            style: TextStyle(fontFamily: 'Pacifico',
                                fontSize: 20,
                                color: Colors.blueGrey),
                          ),
                        ],
                      ),

                      Padding(
                          padding: EdgeInsets.only(left:40),
                          child: Column(
                            children: <Widget>[
                              MaterialButton(
                                onPressed: (){
                                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => RiceHome()));
                                },
                                child: Container(
                                  height:85,
                                  width: 85,
                                  decoration: BoxDecoration(

                                      image: DecorationImage(
                                        image: AssetImage('assets/images/drink.jpg'),
                                      ),
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                          color: Color(0xFF21BFBD),
                                          width: 3,
                                          style: BorderStyle.solid
                                      )
                                  ),
                                ),

                              ),

                              Text(
                                'Drinks',
                                style: TextStyle(fontFamily: 'Pacifico',
                                    fontSize: 20,
                                    color: Colors.blueGrey),
                              ),

                            ],
                          )
                      ),
                    ],

                  ),



                ),

                Padding(
                  padding: EdgeInsets.only(top:20, left:50),
                  child:Row(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          MaterialButton(
                            onPressed: (){
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => RiceHome()));
                            },
                            child: Container(
                              height: 85,
                              width: 85,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                    image: AssetImage('assets/images/cake.jpg'),
                                  ),
                                  borderRadius: BorderRadius.circular(100),
                                  border: Border.all(
                                      color: Color(0xFF21BFBD),
                                      width: 3,
                                      style: BorderStyle.solid
                                  )
                              ),
                            ),

                          ),
                          Text(
                            'Cake',
                            style: TextStyle(fontFamily: 'Pacifico',
                                fontSize: 20,
                                color: Colors.blueGrey),
                          ),
                        ],
                      ),

                      Padding(
                        padding: EdgeInsets.only(left:40),
                        child: Column(
                          children: <Widget>[
                            MaterialButton(
                              onPressed: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) => RiceHome()));
                              },
                              child: Container(
                                height:85,
                                width: 85,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage('assets/images/sweet.jpg'),
                                    ),
                                    borderRadius: BorderRadius.circular(100),
                                    border: Border.all(
                                        color: Color(0xFF21BFBD),
                                        width: 3,
                                        style: BorderStyle.solid
                                    )
                                ),
                              ),

                            ),

                            Text(
                              'Sweets',
                              style: TextStyle(fontFamily: 'Pacifico',
                                  fontSize: 20,
                                  color: Colors.blueGrey),
                            ),

                          ],
                        ),
                      ),
                    ],

                  ),



                ),
              ],
            ),


          ),
        ),

            SizedBox(height: 12.0),



          ]
      ),



    );


  }


}



