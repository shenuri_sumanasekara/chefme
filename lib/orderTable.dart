
import 'package:chefme/arpicoFood.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'model/product.dart';

// ignore: must_be_immutable
class OrderTable extends StatefulWidget {
  List<Product> orderList=[] ;
 OrderTable(this.orderList);


  @override
  _OrderTableState createState() => _OrderTableState(orderList);
}

class _OrderTableState extends State<OrderTable> {
  List<Product> orderList;
  _OrderTableState(this.orderList);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.cyan,
      body:Stack(
        children: <Widget>[
          Container(
            height: 120,
            child:Padding(
              padding: EdgeInsets.fromLTRB(10,30,10,10),
              child: Row(
                children: <Widget>[
                  Align(
                    alignment: Alignment.topLeft,
                    child: IconButton(
                      icon: Icon(Icons.arrow_back,color: Colors.white,),
                      onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ArpicoFood()));
                      },
                    ),
                  ),
                  Spacer(),
                  Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      "My Cart",
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: "Pacifico",
                        fontSize: 24

                      ),
                    ),
                  ),
                  Spacer(),
                  Align(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      icon: Icon(Icons.shopping_cart,color: Colors.white,),
                      onPressed: (){
                        orderList.clear();
                        Navigator.of(context).push(MaterialPageRoute(builder: (context)=>ArpicoFood()));
                      },
                    ),
                  ),

                ],
              ),
            )
          ),
          Padding(
            padding: EdgeInsets.only(top:90),
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius:  BorderRadius.only(topLeft: Radius.circular(35.0),topRight: Radius.circular(35)),

              ),

              child: Column(
                children: <Widget>[
                 Padding(
                   padding: EdgeInsets.only(top: 5,left: 25),
                   child:  Row(
                     children: <Widget>[
                       Text("Shopping Cart",
                         style: TextStyle(
                             fontWeight: FontWeight.bold,
                             fontSize: 18
                         ),),
                   Padding(
                     padding: EdgeInsets.only(top:5,left: 90),
                     child: ButtonTheme(
                       height: 30,
                       minWidth: 50,
                       buttonColor: Colors.green,
                       child: RaisedButton(
                         color: Colors.lightGreen[300],
                         onPressed: (){
                           Navigator.of(context).push(MaterialPageRoute (builder: (context) => ArpicoFood()));
                         },
                         child: Text('Add Item', style: TextStyle(fontFamily: 'Pacifico',
                             fontSize: 20,
                             color: Colors.white),),
                         shape: RoundedRectangleBorder(
                           borderRadius: BorderRadius.circular(30),
                         ),
                       ),
                     ),
                   ),
                     ],
                   ),

                 ),
                  Container(
                    height: 300,
                    padding: EdgeInsets.fromLTRB(25,5,25,5),
                    child: CustomScrollView(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      slivers: <Widget>[
                        SliverList(
                          delegate:SliverChildBuilderDelegate(
                                (BuildContext context, index){
                              return Padding(
                                padding: EdgeInsets.all(20),
                                child:   Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Text(
                                            orderList[index].name,
                                          ),
                                          Spacer(),
                                          Text(
                                            orderList[index].price.toString(),
                                          )
                                        ],



                                      ),
                                    ]),
                              );
                            },
                            childCount: orderList.length,

                          ),

                        ),

                      ],


                    ),
                  ),
                 Padding(
                   padding: EdgeInsets.fromLTRB(35, 0, 25,0),
                    child: ButtonTheme(
                      height: 50,
                      minWidth: 180,
                      buttonColor: Colors.cyan,
                      child: Column(
                        children: <Widget>[
                          RaisedButton(
                            color: Colors.cyan,
                            onPressed: (){
                              orderList.clear();
                              Navigator.of(context).push(MaterialPageRoute(builder:  (context) => ArpicoFood()));
                            },
                            child: Text('Place an Order', style: TextStyle(fontFamily: 'Pacifico',
                                fontSize: 20,
                                color: Colors.white),),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                          ),
                         Padding(
                           padding: EdgeInsets.only(top:20),
                           child:  RaisedButton(
                             color: Colors.cyan,
                             onPressed: (){
                               orderList.clear();
                               Navigator.of(context).push(MaterialPageRoute(builder:  (context) => ArpicoFood()));
                             },
                             child: Text('Clear Cart', style: TextStyle(fontFamily: 'Pacifico',
                                 fontSize: 20,
                                 color: Colors.white),),
                             shape: RoundedRectangleBorder(
                               borderRadius: BorderRadius.circular(30),
                             ),
                           ),
                         )
                        ],
                      )
                    ),

                  ),

                ],
              ),
            ),
          )

        ],
      )

    );
  }
}



