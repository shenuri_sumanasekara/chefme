import 'package:chefme/model/merchant.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'merchantHome.dart';

List<Merchant> groceryList = [];
class Groceries extends StatefulWidget {
  @override
  _GroceriesState createState() => _GroceriesState();
}

class _GroceriesState extends State<Groceries> {
  @override
  // ignore: must_call_super
  void initState() {
    // TODO: implement initState
    getGroceries();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[50],
      body: GridView.builder(
        padding: EdgeInsets.fromLTRB(20, 30, 20, 20),
        scrollDirection: Axis.vertical,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, mainAxisSpacing: 20,
            crossAxisSpacing: 20),
        itemBuilder:(BuildContext context, int index) {
          return MaterialButton(
            height: 250,
            shape: RoundedRectangleBorder(
              borderRadius:  BorderRadius.circular(18),
            ),
            //padding: EdgeInsets.all(20),
            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => MerchantHome()));
            },
            color: Colors.white,
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Container(
                    child: Image.network(groceryList[index].img,
                      height: 90,
                      width: 90,),
//                      height: 90,
//                      width: 90,
//                      decoration: BoxDecoration(
//                          image: DecorationImage(
//                            image: AssetImage(merchantList[index].img),
//                          ),
//                          borderRadius: BorderRadius.circular(100),
//                          border: Border.all(
//                              color: Color(0xFF21BFBD),
//                              width: 3,
//                              style: BorderStyle.solid
//                          )
//                      ),
                  ),
                  Padding(
                      padding:EdgeInsets.only(top:20),
                      child:Text(groceryList[index].name,
                        style: TextStyle(fontSize: 20,
                            color: Colors.blueGrey),)
                  ),

                ]
            ),
          );
        },
        itemCount:groceryList.length,
      ),

    );
  }
}



Future<void> getGroceries(){
  return Firestore.instance.collection('merchant').where('category', isEqualTo:'grocery' ).getDocuments().then((querySnapShot) {
    groceryList.clear();
    querySnapShot.documents.forEach((element) {
      Merchant merchant = Merchant.fromMap(element.data);
      groceryList.add(merchant);
    });

  });
}