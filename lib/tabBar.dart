import 'dart:ui';

import 'package:chefme/fastFood.dart';
import 'package:flutter/material.dart';
import 'merchantHome.dart' as merchantHome;
import 'addRecipe.dart' as addRecipe;
import 'fastFood.dart' as fastFood;
import 'groceries.dart' as groceries;


class  TabBars extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length:3,
      child: Scaffold(
      appBar: AppBar(
        title: Text('Chef Me',
          textAlign: TextAlign.center,
          style: TextStyle(fontFamily: 'Pacifico',
              fontSize: 24,
              color: Colors.white),
        ),

        backgroundColor: Colors.blue,
        bottom: TabBar(
          isScrollable: true,
          unselectedLabelColor: Colors.white,
          labelColor: Colors.black26,
          tabs: <Tab>[
            Tab(
              text: "All Categories",
              icon: Icon(IconData(58819, fontFamily: 'MaterialIcons',)),
            ),
            Tab(
              text: "Groceries",
              icon: Icon(IconData( 59596, fontFamily: 'MaterialIcons')),
            ),
            Tab(
              text: "Fast Food",
              icon: Icon(IconData(58746, fontFamily: 'MaterialIcons')),
            ),

          ],

        ),

      ),
      body: TabBarView(
        children: <Widget>[
          merchantHome.MerchantHome(),
          groceries.Groceries(),
          fastFood.FastFood(),
        ],

      ),
    ),
    );
  }
}





//  import 'package:flutter/material.dart';
//  import 'merchantHome.dart' as merchantHome;
//  import 'addRecipe.dart' as addRecipe;
//  import 'home.dart' as home;
//
//
//  class  TabBars extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//  return Scaffold(
//  body: DefaultTabController(
//  length: 4,
////      appBar: AppBar(
////        backgroundColor: Colors.blue,
//  child: NestedScrollView(
//  headerSliverBuilder:
//  (BuildContext context, bool innerBoxIsScrolled)
//  {
//  return <Widget>[
//  SliverOverlapAbsorber(
//  handle:
//  NestedScrollView.sliverOverlapAbsorberHandleFor(context),
//  child: SliverSafeArea(
//  top: false,
//  sliver: SliverAppBar(
//  title: Text('Tab Demo'),
//  floating: true,
//  pinned: true,
//  snap: true,
//  forceElevated: innerBoxIsScrolled,
//  bottom: TabBar(
//  isScrollable: true,
//  unselectedLabelColor: Colors.white,
//  labelColor: Colors.black26,
//  tabs: <Tab>[
//  Tab(
//  text: "All",
//  icon: Icon(IconData(58819, fontFamily: 'MaterialIcons')),
//  ),
//  Tab(
//  text: "Groceries",
//  icon: Icon(IconData(59596, fontFamily: 'MaterialIcons')),
//  ),
//  Tab(
//  text: "Fast Food",
//  icon: Icon(IconData(58746, fontFamily: 'MaterialIcons')),
//  ),
//  Tab(
//  text: "Cakes",
//  icon: Icon(IconData(59369, fontFamily: 'MaterialIcons')),
//  ),
//
//
//  ],
//
//  ),
//
//  ),
//
//  ),
//  ),
//
//  ];
//  },
//  body: TabBarView(
//  children: <Widget>[
//  merchantHome.MerchantHome(),
//  addRecipe.AddRecipe(),
//  home.Home(),
//  addRecipe.AddRecipe(),
//  ],
//
//  ),
//
//  ),
//  ),
//  );
//  }
//  }
