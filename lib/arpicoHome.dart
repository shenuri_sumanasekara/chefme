
import 'package:chefme/model/category.dart';
import 'package:chefme/upBar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import "package:flutter/material.dart";

class ArpicoHome extends StatefulWidget {
  @override
  _ArpicoHomeState createState() => _ArpicoHomeState();

}
List<Category> categoryList =[];
List<String> categoryLists=<String>['Vegetagles','Fruits','Bath Items'];
class _ArpicoHomeState extends State<ArpicoHome> {
  @override
  void initState() {
   //getCategory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     body: ListView(

       children: <Widget>[
         Container(
               height: 120,
               decoration: BoxDecoration(
                 color: Colors.blue[200],
                 borderRadius: BorderRadius.only(bottomRight: Radius.circular(45),bottomLeft: Radius.circular(45) ),
               ),
               child:ListView(
                 children: <Widget>[
                   Padding(
                     padding: EdgeInsets.only(top:10,left:15),
                     child:  Row(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: <Widget>[
                         IconButton(
                             icon: Icon(Icons.arrow_back),
                             color: Colors.white,
                             onPressed:() {
                               Navigator.of(context).push(
                                   MaterialPageRoute(builder: (context) => UpBars()));

                             }
                         ),
                         Padding(
                           padding: EdgeInsets.only(left:70,right: 70),
                           child: Text(
                             'Arpico ',
                             style: TextStyle(
                                 fontSize: 24,
                                 color: Colors.white,
                                 fontFamily: 'Pacifico'
                             ),
                           ),
                         ),
                         Padding(
                           padding: EdgeInsets.only(left:20),
                           child: IconButton(
                             onPressed: (){},
                             icon: Icon(Icons.shopping_cart),
                             color: Colors.white70,
                             iconSize: 24,
                           ),

                         )
                       ],

                     ),
                   ),

                   Padding(
                     padding: EdgeInsets.fromLTRB(80, 10,80,10),
                     child: ChoiceDropDown(),
                   )
                 ],
               )
           ),
           Container(

           )
       ],

     ),
    );
  }
}

Widget ChoiceDropDown(){

  String dropdownValue= 'one';
  return Container(
    height:40,
    child:Container(
      decoration: BoxDecoration(
        color: Colors.white,
            borderRadius: BorderRadius.circular(8),
      ),
      child:  Row(
        children: <Widget>[
          Spacer(),
//        Text(
//            'Select Category',
//            style:TextStyle(
//              fontSize: 14,
//              color: Colors.blueGrey,
//            ),
//          ),
          Spacer(),
          DropdownButton(
                  hint: Text('Select Category'),
                  items: categoryList.map<DropdownMenuItem>((value) =>new DropdownMenuItem(
                     child: Text(value.name,
                       style: TextStyle(
                               color: Colors.blue
                       ),
                     ),
                           value: value.name,
                         )).toList(),
                ),

          Spacer(),
          IconButton(
            icon: Icon(Icons.arrow_drop_down),
            color: Colors.blueGrey,
            iconSize: 30,
            padding: EdgeInsets.only(top:1),
            onPressed: (){
              getCategory();
            },
          ),

        ],
      ),
    )
  );
}


Future<void>getCategory(){
  final firestoreInsance = Firestore.instance;
  return firestoreInsance.collection('category').getDocuments().then((value){
  categoryList.clear();
      value.documents.forEach((element) {
        print(element.data);
         Category category = Category.fromMap(element.data);
         categoryList.add(category);

  });});
}

