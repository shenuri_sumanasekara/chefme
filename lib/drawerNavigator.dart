import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'model/merchant.dart';

Merchant merchant;

class NavDrawer extends StatelessWidget {
  String uid;

  NavDrawer({this.uid});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Stack(
        children: <Widget>[
          DrawerHeader(
            decoration: BoxDecoration(
              color: Color(0xFF7A9BEE),
              borderRadius: BorderRadius.only(
                  bottomRight: Radius.elliptical(175, 175)),
            ),

          ),
          Positioned(
            top: 35,
            left: 5,
            child: Container(
              height: 90,
              width: 90,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/images/logingirl.jpg")
                  ),
                  borderRadius: BorderRadius.circular(100),
                  border: Border.all(
                      color: Colors.white,
                      width: 3,
                      style: BorderStyle.solid
                  )
              ),
            ),
          ),
          widgetGetUserDetails(context),
          ListTile(
            contentPadding: EdgeInsets.fromLTRB(10, 200, 0, 0),
            leading: Icon(Icons.person),
            title: Text(
              'Order for a Friend',
              style: TextStyle(fontSize: 17, color: Colors.blueGrey),
            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.fromLTRB(10, 245, 0, 0),
            leading: Icon(Icons.card_giftcard),
            title: Text(
              'Free Food',
              style: TextStyle(fontSize: 17, color: Colors.blueGrey),

            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.fromLTRB(10, 290, 0, 0),
            leading: Icon(Icons.payment),
            title: Text(
              'Payment',
              style: TextStyle(fontSize: 17, color: Colors.blueGrey),
            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.fromLTRB(10, 335, 0, 0),
            leading: Icon(Icons.directions_car),
            title: Text(
              'My Orders',
              style: TextStyle(fontSize: 17, color: Colors.blueGrey),
            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.fromLTRB(10, 380, 0, 0),
            leading: Icon(Icons.bookmark),
            title: Text(
              'Order Later',
              style: TextStyle(fontSize: 17, color: Colors.blueGrey),
            ),
          ),
          ListTile(
            contentPadding: EdgeInsets.fromLTRB(10, 425, 0, 0),
            leading: Icon(Icons.add_alert),
            title: Text(
              'Emergency',
              style: TextStyle(fontSize: 17, color: Colors.blueGrey),
            ),
          ),

          ListTile(
            contentPadding: EdgeInsets.fromLTRB(10, 470, 0, 0),
            leading: Icon(Icons.settings),
            title: Text(
              'Settings',
              style: TextStyle(fontSize: 17, color: Colors.blueGrey),
            ),
          ),

          ListTile(
            contentPadding: EdgeInsets.fromLTRB(10, 515, 0, 0),
            leading: Icon(Icons.info),
            title: Text(
              'About Us',
              style: TextStyle(fontSize: 17, color: Colors.blueGrey),
            ),
          ),

        ],


      ),

    );
  }

  Widget widgetGetUserDetails(BuildContext context) {
    return new StreamBuilder(
        stream: Firestore.instance.collection("User").document(uid).snapshots(),
        builder: (context, snapshot) {
          var userDetails = snapshot.data;
          return Column(
              children: <Widget>[
               Padding(
                 padding: EdgeInsets.only(top: 50,left: 100),
                 child:  Row(
                   children: <Widget>[
                     Text(userDetails["firstName"],
                         style: TextStyle(
                           fontFamily: 'Pacifico',
                           fontSize: 18,
                           color: Colors.white,
                         )

                     ),
                     Padding(
                       padding: EdgeInsets.only(left: 7),
                       child: Text(userDetails["lastName"],
                           style: TextStyle(
                             fontFamily: 'Pacifico',
                             fontSize: 18,
                             color: Colors.white,
                           )
                       ),
                     )
                   ],
                 ),
               ),

                 Text(userDetails["email"],
                  style: TextStyle(
                  fontSize: 14,
                  color: Colors.white,
          ),
                ),

              ],

          );
        }

    );
  }
}