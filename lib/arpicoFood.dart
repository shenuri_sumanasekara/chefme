import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'addRice.dart';
import 'model/product.dart';
import 'orderTable.dart';

List<Product> productList = [];
List<Product> orderList = [];

class ArpicoFood extends StatefulWidget {
  @override
  _ArpicoFoodState createState() => _ArpicoFoodState();
}

class _ArpicoFoodState extends State<ArpicoFood> {




  @override
  void initState() {
    getVegetables();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Stack(
        children: <Widget>[
          CustomScrollView(
            slivers: <Widget>[
              SliverGrid(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2,
                  mainAxisSpacing: 10,
                  crossAxisSpacing: 10,
                ),
                delegate: SliverChildBuilderDelegate(
                      (BuildContext context, index) {
                    return Padding(
                        padding: EdgeInsets.only(top: 20, left: 20, right: 20,),
                        child: MaterialButton(
                          height: 250,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18),
                          ),

                          onPressed: () {},
                          color: Colors.white,
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[

                                Container(
                                  child: Image.network(productList[index].img,
                                    height: 70,
                                    width: 70,),
                                ),
                                Padding(
                                    padding: EdgeInsets.only(top: 10),
                                    child: Text(productList[index].name,
                                      style: TextStyle(fontSize: 20,
                                          color: Colors.blueGrey),)
                                ),
                                Align(
                                  alignment: Alignment.bottomLeft,
                                  child: IconButton(
                                    icon: Icon(Icons.add_circle),
                                    color: Colors.cyan.shade400,
                                    iconSize: 30,
                                    onPressed: () {

                                      orderList.add(productList[index]);

                                      //AddRice(productList[index])));

                                    },
                                  ),
                                ),


                              ]
                          ),

                        ));
                  },
                  childCount: productList.length,

                ),



              ),

            ],

          ),

         Align(
           alignment: Alignment.bottomCenter,
           child:  RaisedButton(
             child: Text(
                 "View Cart"
             ),
             onPressed: (){
               Navigator.of(context).push(MaterialPageRoute (builder: (context)=>
                   OrderTable(orderList))
                  // OrderTable(productList[index].docId,productList[index].name,productList[index].price))
               );
             },
           ),
         )
        ],
      )

    );
  }
}
//
//class ArpicoFood extends StatefulWidget {
//
//  @override
//  _ArpicoFoodState createState() => _ArpicoFoodState();
//}
//
//class _ArpicoFoodState extends State<ArpicoFood> {
////  final ValueSetter<Product> _valueSetter;
////  _ArpicoFoodState(this._valueSetter);
//
//
//  @override
//  void initState() {
//    getVegetables();
//    super.initState();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      body: CustomScrollView(
//        slivers: <Widget>[
//          SliverGrid(
//            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
//              crossAxisCount: 2,
//              mainAxisSpacing: 10,
//              crossAxisSpacing: 10,
//            ),
//            delegate: SliverChildBuilderDelegate(
//                  (BuildContext context, index) {
//                return Padding(
//                  padding: EdgeInsets.only(top: 20, left: 20, right: 20,),
//                  child: MaterialButton(
//                    height: 250,
//                    shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.circular(18),
//                    ),
//
//                    onPressed: () {},
//                    color: Colors.white,
//                    child: Column(
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//
//                          Container(
//                            child: Image.network(productList[index].img,
//                              height: 70,
//                              width: 70,),
//                          ),
//                          Padding(
//                              padding: EdgeInsets.only(top: 10),
//                              child: Text(productList[index].name,
//                                style: TextStyle(fontSize: 20,
//                                    color: Colors.blueGrey),)
//                          ),
//                          Align(
//                            alignment: Alignment.bottomLeft,
//                            child: IconButton(
//                              icon: Icon(Icons.add_circle),
//                              color: Colors.cyan.shade400,
//                              iconSize: 30,
//                              onPressed: () {
//
//                                Navigator.of(context).push(MaterialPageRoute(builder:(context) =>
//                                   OrderTable(productList[index].docId,productList[index].name,productList[index].price)));
//                               // AddRice(productList[index])));
//
//                              },
//                            ),
//                          )
//                        ]
//                    ),
//                  ),
//                );
//              },
//              childCount: productList.length,
//            ),
//
//          ),
////          RaisedButton(
////            onPressed: (){
////              Navigator.of(context).push(MaterialPageRoute(builder:(context) =>
////                  OrderTable(productList[index].docId,productList[index].name,productList[index].price)));
////
////            },
//          //)
//        ],
//      ),
//    );
//  }
//
//
//}



Future<void> getVegetables(){
  final fireStoreInstance = Firestore.instance;
  return fireStoreInstance.collection('products').orderBy('name').getDocuments().then((querySnapShot) => {
        productList.clear(),
        querySnapShot.documents.forEach((element) {
          Product product = Product.fromMap(element.data);
          print(element.data);
          productList.add(product);
        })

  });
}